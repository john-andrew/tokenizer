/*
 * John M
 * CS: Systems Programming 214 
 *
 * tokenizer.c
 */

#define pRED "\x1B[31m"
#define pRESET "\x1b[0m"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//--------------------[START: DATA TYPES]--------------------//

/*
 Enum type "tokenState"
 */
enum tokenState_ {
    START,

    DEC,
    ERROR,
    FLOAT,
    FLOATD,
    FLOATE,
    HEX,
    OCTAL,
    OPERATOR,
    WHITESPACE,
    WORD,
    ZERO,
    ZEROX,
    
    ENDOFSTRING
};
typedef enum tokenState_ tokenState;

/*
 * Tokenizer type.  You need to fill in the type as part of your implementation.
 */
struct TokenizerT_ {
    int i;
    char* string;
    char* tokenString;
    tokenState state;

};
typedef struct TokenizerT_ TokenizerT;

//--------------------[END: DATA TYPES]--------------------//




//--------------------[START: HELPER FUNCTIONS]--------------------//

/*
 * createOperatorToken
 * FUNCTION: creates a token out of given operator chars
 * INPUTS: chars that comprise the token; second is 0
 *   if operator is only one char
 * MODIFIES: nothing
 * MEMORY: allocates memory for token; must be freed after printed
 * RETURNS: a token consisting of the operator(s), NULL otherwise
 */
char* createOperatorToken (char c1, char c2) {
    char* opTokenRet;
    if (c2 != 0) {
        opTokenRet = (char*)malloc(sizeof(char) * 
                                         (2 + 1));
        if (opTokenRet == NULL) {
            printf("ERROR: In createOperatorToken, unable to alloc memory to token\n");
            return NULL;
        }
        char dummy[3] = {c1, c2, '\0'};
        strcpy(opTokenRet, dummy);
    }
    else {
        opTokenRet = (char*)malloc(sizeof(char) * 
                                         (1 + 1));
        if (opTokenRet == NULL) {
            printf("ERROR: In createOperatorToken, unable to alloc memory to token\n");
            return NULL;
        }
        char dummy[2] = {c1, '\0'};
        strcpy(opTokenRet, dummy);
    }
    return opTokenRet;
}

/*
 * createOperatorTokenType
 * FUNCTION: specifies tokeType out of given operator chars
 * INPUTS: chars that comprise the token; second is 0
 *   if operator is only one char
 * MEMORY: allocates memory for tokentype string; must be freed after printed
 * RETURNS: a string identifying the type of token, NULL otherwise
 */
char* createOperatorTokenType (char c1, char c2) {

    char* opToken;
    if ( (c1 == '+') && (c2 == '+') ) {
        opToken = "increment";
    }
    else if ( (c1 == '-') && (c2 == '-') ) {
        opToken = "decrement";
    }
    else if ( (c1 == '<') && (c2 == '<') ) {
        opToken = "shift left";
    }
    else if ( (c1 == '>') && (c2 == '>') ) {
        opToken = "shift right";
    }
    else if ( (c1 == '=') && (c2 == '=') ) {
        opToken = "equals";
    }
    else if ( (c1 == '!') && (c2 == '=') ) {
        opToken = "not equals";
    }
    else if ( (c1 == '<') && (c2 == '=') ) {
        opToken = "less or equals";
    }
    else if ( (c1 == '>') && (c2 == '=') ) {
        opToken = "greater or equal";
    }
    else if ( (c1 == '&') && (c2 == '&') ) {
        opToken = "logical and";
    }
    else if ( (c1 == '|') && (c2 == '|') ) {
        opToken = "logical or";
    }
    else if ( (c1 == '+') && (c2 == '=') ) {
        opToken = "plus equals";
    }
    else if ( (c1 == '-') && (c2 == '=') ) {
        opToken = "minus equals";
    }
    else if ( (c1 == '*') && (c2 == '=') ) {
        opToken = "times equals";
    }
    else if ( (c1 == '/') && (c2 == '=') ) {
        opToken = "div equals";
    }
    else if ( (c1 == '%') && (c2 == '=') ) {
        opToken = "modulus equals";
    }
    else if ( (c1 == '&') && (c2 == '=') ) {
        opToken = "address equals";
    }
    else if ( (c1 == '^') && (c2 == '=') ) {
        opToken = "bitwise-exq or equals";
    }
    else if ( (c1 == '|') && (c2 == '=') ) {
        opToken = "bitwise or equals";
    }
    else if ( (c1 == '-') && (c2 == '>') ) {
        opToken = "struct ptr";
    }
    else if (c1 == '(') {
        opToken = "left parens";
    }
    else if (c1 == ')') {
        opToken = "right parens";
    }
    else if (c1 == '[') {
        opToken = "left bracket";
    }
    else if (c1 == ']') {
        opToken = "right bracket";
    }
    else if (c1 == '*') {
        opToken = "multiply/indirect";
    }
    else if (c1 == '&') {
        opToken = "address";
    }
    else if (c1 == '-') {
        opToken = "minus";
    }
    else if (c1 == '!') {
        opToken = "negate";
    }
    else if (c1 == '/') {
        opToken = "divide";
    }
    else if (c1 == '%') {
        opToken = "modulus";
    }
    else if (c1 == '+') {
        opToken = "plus";
    }
    else if (c1 == '-') {
        opToken = "minus";
    }
    else if (c1 == '<') {
        opToken = "less than";
    }
    else if (c1 == '>') {
        opToken = "greater than";
    }
    else if (c1 == '^') {
        opToken = "bitwise exlusive or";
    }
    else if (c1 == '|') {
        opToken = "bitwise or";
    }
    else if (c1 == '?') {
        opToken = "condition";
    }
    else if (c1 == ':') {
        opToken = "true-false";
    }
    else if (c1 == ',') {
        opToken = "comma operator";
    }
    else if (c1 == '=') {
        opToken = "equals assignment";
    }
    else if (c1 == '.') {
        opToken = "struct member";
    }
    if (opToken == NULL) {
        printf("ERROR: Valid operator not found\n");
        return NULL;
    }

    char* opTokenRet = (char*)malloc(sizeof(char) * 
                                    (strlen(opToken) + 1));
    if (opTokenRet == NULL) {
        printf("ERROR: In createOperatorTokenType, unable to alloc memory to string\n");
        return NULL;
    }
    strcpy(opTokenRet, opToken);

    return opTokenRet;
}

/*
 * isOperator
 * FUNCTION: Determines if one or two chars is an operator
 * INPUTS: two chars, as some operators are more than one char
 * MODIFIES: Nothing
 * RETURNS: 2 if a length-2 operator, 1 if length-1, 0 otherwise
 */
int isOperator (char c1, char c2) {
    if ( (c1 == '+') && (c2 == '+') ) {
        return 2;
    }
    if ( (c1 == '-') && (c2 == '-') ) {
        return 2;
    }
    if ( (c1 == '<') && (c2 == '<') ) {
        return 2;
    }
    if ( (c1 == '>') && (c2 == '>') ) {
        return 2;
    }
    if ( (c1 == '=') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '!') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '<') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '>') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '&') && (c2 == '&') ) {
        return 2;
    }
    if ( (c1 == '|') && (c2 == '|') ) {
        return 2;
    }
    if ( (c1 == '+') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '-') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '*') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '/') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '%') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '&') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '^') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '|') && (c2 == '=') ) {
        return 2;
    }
    if ( (c1 == '-') && (c2 == '>') ) {
        return 2;
    }
    if (c1 == '(') {
        return 1;
    }
    if (c1 == ')') {
        return 1;
    }
    if (c1 == '[') {
        return 1;
    }
    if (c1 == ']') {
        return 1;
    }
    if (c1 == '*') {
        return 1;
    }
    if (c1 == '&') {
        return 1;
    }
    if (c1 == '-') {
        return 1;
    }
    if (c1 == '!') {
        return 1;
    }
    if (c1 == '/') {
        return 1;
    }
    if (c1 == '%') {
        return 1;
    }
    if (c1 == '+') {
        return 1;
    }
    if (c1 == '-') {
        return 1;
    }
    if (c1 == '<') {
        return 1;
    }
    if (c1 == '>') {
        return 1;
    }
    if (c1 == '^') {
        return 1;
    }
    if (c1 == '|') {
        return 1;
    }
    if (c1 == '?') {
        return 1;
    }
    if (c1 == ':') {
        return 1;
    }
    if (c1 == ',') {
        return 1;
    }
    if (c1 == '=') {
        return 1;
    }
    if (c1 == '.') {
        return 1;
    }
    return 0;
}

/*
 * isOctal
 * FUNCTION: Determines if a char is an octal digit 0-7
 * INPUTS: char being checked
 * MODIFIES: Nothing
 * RETURNS: 1 if a valid octal digit, 0 otherwise
 */
int isOctal (char c) {
    char* validChars = "01234567";
    if (strchr(validChars, c)) {
        return 1;
    }
    return 0;
}

/*
 * isHex
 * FUNCTION: Determines if a char is an hex digit 0-9, a-e, A-E
 * INPUTS: char being checked
 * MODIFIES: Nothing
 * RETURNS: 1 if a valid hex digit, 0 otherwise
 */
int isHex (char c) {
    char* validChars = "0123456789abcdefABCDEF";
    if (strchr(validChars, c)) {
        return 1;
    }
    return 0;
}

/*
 * isXAndHex
 * FUNCTION: Determines if one char is an X or X, and another
 *     a hex digit 0-9 or A-E,a-e
 * INPUTS: first and second char
 * MODIFIES: Nothing
 * RETURNS: 1 if both cases hold, 0 otherwise
 */
int isXAndHex (char c1, char c2) {
    if (c2 == '\0') {
        return 0;
    }
    if ( (c1 == 'x') || (c1 == 'X') ) {
        if (isHex(c2)) {
            return 1;
        }
    }
    return 0;
}

/*
 * isDigit
 * FUNCTION: Determines if a char is a decimal digit 0-9
 * INPUTS: char being checked
 * MODIFIES: Nothing
 * RETURNS: 1 if a digit char, 0 otherwise
 */
int isDigit (char c) {
    char* validChars = "0123456789";
    if (strchr(validChars, c)) {
        return 1;
    }
    return 0; 
}

/*
 * isPlusMinusAndDigit
 * FUNCTION: Determines if one char is a +,- and another
 *     a decimal digit 0-9
 * INPUTS: first and second char
 * MODIFIES: Nothing
 * RETURNS: 1 if both cases hold, 0 otherwise
 */
int isPlusMinusAndDigit (char c1, char c2) {
    if (c2 == '\0') {
        return 0;
    }
    if ( (c1 == '+') || (c1 == '-') ) {
        if (isDigit(c2)) {
            return 1;
        }
    }
    return 0;
}

/*
 * isDotAndDigit
 * FUNCTION: Determines if one char a decimal dot, and another
 *     a decimal digit 0-9
 * INPUTS: first and second char
 * MODIFIES: Nothing
 * RETURNS: 1 if both cases hold, 0 otherwise
 */
int isDotAndDigit (char c1, char c2) {
    if (c2 == '\0') {
        return 0;
    }
    if ( (c1 == '.') && (isDigit(c2)) ) {
        return 1;
    }
    return 0;
}

/*
 * isABC
 * FUNCTION: Determines if a char is an alphabetical char
 * INPUTS: char being checked
 * MODIFIES: Nothing
 * RETURNS: 1 if an alphabetical char, 0 otherwise
 */
int isABC (char c) {
    char* validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (strchr (validChars, c)) {
        return 1;
    }
    return 0;
}

/*
 * isWhitespace
 * FUNCTION: Determines if a char is a whitespace character
 * INPUTS: char being checked
 * MODIFIES: Nothing
 * RETURNS: 1 if a whitespace char, 0 otherwise
 */
int isWhitespace (char c) {
    if (c == ' ') {
        return 1;
    }
    else if ( (c >= 9) && (c <= 13) ) {
        return 1;
    }
    return 0;
 }

/*
 * tokenReachedEnd
 * FUNCTION: Determines if token is already at end of string,
 *   used to terminate a repeating state
 * INPUTS: Tokenizer object
 * MODIFIES: Nothing
 * RETURNS: 1 if true, 0 otherwise
 */
int tokenReachedEnd(TokenizerT* tk) {
    if ((tk->i + 1) >= strlen(tk->string)) {
        return 1;
    }
    return 0;
}

/*
 * createErrorToken
 * FUNCTION: Creates error token to displayed, including conversion
 *   of escape char errors into bracketed hex form
 * INPUTS: char containing escape/error char
 * MODIFIES: Nothing
 * RETURNS: An error token string, NULL otherwise
 * MEMORY: allocates memory for token; must be freed after printed
 */
 char* createErrorToken (char c) {
    #ifdef DEBUG
        printf("**ENTERING createErrorToken()**\n");
    #endif

    char* errorToken = malloc(sizeof(char) *
                       (7 + 1));
    if (errorToken == NULL) {
        printf("ERROR: In createErrorToken, unable to allocate"
               " memory for errorToken\n");
        return NULL;
    }
    strcpy(errorToken, "(error)");

    char* hexStr = NULL;
    if (c == 0) {
        hexStr = "[0x00]";
    }
    else if (c == 1) {
        hexStr = "[0x01]";
    }
    else if (c == 2) {
        hexStr = "[0x02]";
    }
    else if (c == 3) {
        hexStr = "[0x03]";
    }
    else if (c == 4) {
        hexStr = "[0x04]";
    }
    else if (c == 5) {
        hexStr = "[0x05]";
    }
    else if (c == 6) {
        hexStr = "[0x06]";
    }
    else if (c == 7) {
        hexStr = "[0x07]";
    }
    else if (c == 8) {
        hexStr = "[0x08]";
    }
    else if (c == 14) {
        hexStr = "[0x0E]";
    }
    else if (c == 15) {
        hexStr = "[0x0F]";
    }
    else if (c == 16) {
        hexStr = "[0x10]";
    }
    else if (c == 17) {
        hexStr = "[0x11]";
    }
    else if (c == 18) {
        hexStr = "[0x12]";
    }
    else if (c == 19) {
        hexStr = "[0x13]";
    }
    else if (c == 20) {
        hexStr = "[0x14]";
    }
    else if (c == 21) {
        hexStr = "[0x15]";
    }
    else if (c == 22) {
        hexStr = "[0x16]";
    }
    else if (c == 23) {
        hexStr = "[0x17]";
    }
    else if (c == 24) {
        hexStr = "[0x18]";
    }
    else if (c == 25) {
        hexStr = "[0x19]";
    }
    else if (c == 26) {
        hexStr = "[0x1A]";
    }
    else if (c == 27) {
        hexStr = "[0x1B]";
    }
    else if (c == 28) {
        hexStr = "[0x1C]";
    }
    else if (c == 29) {
        hexStr = "[0x1D]";
    }
    else if (c == 30) {
        hexStr = "[0x1E]";
    }
    else if (c == 31) {
        hexStr = "[0x1F]";
    }
    else if (c == 92) {
        hexStr = "[0x5C]";
    }
    else if (c == 39) {
        hexStr = "[0x27]";
    }
    else if (c == 34) {
        hexStr = "[0x22]";
    }
    else if (c == 63) {
        hexStr = "[0x3F]";
    }

    if (hexStr != NULL) {
        #ifdef DEBUG
            printf("case hexStr: %s\n", hexStr);
        #endif

        errorToken = realloc(errorToken, sizeof(char) *
                                                (13 + 2));
        if (errorToken == NULL) {
            printf("ERROR: In createErrorToken, unable to realloc"
                   " memory for errorToken\n");
            #ifdef DEBUG
                printf("**EXITING createErrorToken()**\n");
            #endif
            return NULL;
        }
        strcat(errorToken, hexStr);
    }

    #ifdef DEBUG
        printf("**EXITING createErrorToken()**\n");
    #endif
    return errorToken;
 }


/*
 * setNewTokenState
 * FUNCTION: Sets new "state" of a TokenizerT based on the char
 *   of the current index, for beginning a new token 
 * INPUTS: TokenizerT whose state will be updated
 * MODIFIES: ->state
 * RETURNS: Nothing
 */
void setNewTokenState(TokenizerT* tk) {
    #ifdef DEBUG
        printf("**ENTERING setNewTokenState()**\n");
    #endif

    int i = tk->i;
    char* str = tk->string;

    //ENDOFSTRING
    if (i >= strlen(str)) {
        tk->state = ENDOFSTRING;

        #ifdef DEBUG
            printf("{{ ENDOFSTRING state, reached end of string }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }

    char c = str[i];
    char c2 = str[i+1];

    //DEC
    if (strchr("123456789", c)) {
        tk->state = DEC;

        #ifdef DEBUG
            printf("{{ DEC state }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }
    //WHITESPACE
    else if (isWhitespace(c)) {
        tk->state = WHITESPACE;

        #ifdef DEBUG
            printf("{{ WHITESPACE state }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }
    //WORD
    else if (isABC(c)) {
        tk->state = WORD;

        #ifdef DEBUG
            printf("{{ WORD state }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }
    //ZERO
    else if (c == '0') {
        tk->state = ZERO;

        #ifdef DEBUG
            printf("{{ ZERO state }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }
    //OPERATOR
    else if (isOperator(c, c2)) {
        tk->state = OPERATOR;
        #ifdef DEBUG
            printf("{{ OPERATOR state }}\n");
            printf("**EXITING setNewTokenState()**\n");
        #endif
        return;
    }

    //ERROR
    tk->state = ERROR;
    #ifdef DEBUG
        printf("{{ ERROR state }}\n");
        printf("**EXITING setNewTokenState()**\n");
    #endif
    return;
}

void printToken(const char* type, TokenizerT* tk) {
    //printf("%s \"%s\"\n", type, tk->tokenString);
    //printf("\x1B[31m%s \"%s\"\n\x1B[0m", type, tk->tokenString);
    printf(pRED "%s \"%s\"\n" pRESET, type, tk->tokenString);
}

//--------------------[END: HELPER FUNCTIONS]--------------------//



/*
 * TKCreate creates a new TokenizerT object for a given token stream
 * (given as a string).
 * 
 * TKCreate should copy the arguments so that it is not dependent on
 * them staying immutable after returning.  (In the future, this may change
 * to increase efficiency.)
 *
 * If the function succeeds, it returns a non-NULL TokenizerT.
 * Else it returns NULL.
 *
 * You need to fill in this function as part of your implementation.
 */
TokenizerT *TKCreate( char * ts ) {

    #ifdef DEBUG
        printf("**ENTERING TKCreate()**\n");
    #endif

    if (ts == NULL) {
        printf("ERROR: TKCreate received a NULL string\n");
        return NULL;
    }

    TokenizerT* tk = (TokenizerT*)malloc(sizeof(TokenizerT));
    if (tk == NULL) {
        printf("ERROR: TKCreate is unable to allocate memory for tk\n");
        return NULL;
    } 

    tk->string = (char*)malloc(sizeof(char) *
                              (strlen(ts) + 1));
    if (tk->string == NULL) {
        printf("ERROR: TKCreate is unable to allocate memory for tk->string");
        free(tk);
        return NULL;
    }

    strcpy(tk->string, ts);
    tk->i = 0;
    setNewTokenState(tk);

    #ifdef DEBUG
        int size;
        size = sizeof(*tk);
        printf("{{ tk size: %d}}\n", size);
        printf("{{ tk->string: %s}}\n", tk->string);
        size = strlen(tk->string);
        printf("{{ tk->string len: %d}}\n", size);
        printf("**EXITING TKCreate()**\n");
    #endif
    return tk;
}




/*
 * TKDestroy destroys a TokenizerT object.  It should free all dynamically
 * allocated memory that is part of the object being destroyed.
 *
 * You need to fill in this function as part of your implementation.
 */
void TKDestroy( TokenizerT * tk ) {
    #ifdef DEBUG
        printf("**ENTERING TKDestroy()**\n");
    #endif

    free(tk->string);
    free(tk);

    #ifdef DEBUG
        printf("**EXITING TKDestroy()**\n");
    #endif
    return;
}




/*
 * TKGetNextToken returns the next token from the token stream as a
 * character string.  Space for the returned token should be dynamically
 * allocated.  The caller is responsible for freeing the space once it is
 * no longer needed.
 *
 * If the function succeeds, it returns a C string (delimited by '\0')
 * containing the token.  Else it returns 0.
 *
 * You need to fill in this function as part of your implementation.
 */
char *TKGetNextToken( TokenizerT * tk ) {
    #ifdef DEBUG
        printf("**ENTERING TKGetNextToken()**\n");
    #endif

    if (tk == NULL) {
        printf("ERROR: In TKGetNextToken, tk not initialized\n");
        return NULL;
    }

    setNewTokenState(tk); //can uninitialize this in TKCreate, TO-DO
    if (tk->state == ENDOFSTRING) {
        #ifdef DEBUG
        printf("{{ tk->state: ENDOFSTRING }}\n");
        printf("**EXITING TKGetNextToken()**\n");
        #endif
        return 0;
    }

    int tokenStart = tk->i;
    int nextTokenDistance = 1;

    int tokenLoop = 1;
    while(tokenLoop) {
        //WHITESPACE
        if (tk->state == WHITESPACE) {
            #ifdef DEBUG
                printf("{{ whitespace case, i: %d }}\n", tk->i);
                printf("**EXITING TKGetNextToken()**\n");
            #endif
            tk->i += 1;
            return "";
        }
        //ERROR
        else if (tk->state == ERROR) {
            #ifdef DEBUG
                printf("{{ error case, i: %d }}\n", tk->i);
                printf("**EXITING TKGetNextToken()**\n");
            #endif
            tk->i += 1;
            return createErrorToken(tk->string[tk->i - 1]);
        }
        //WORD
        else if (tk->state == WORD) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ word->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = WORD;
                break;
            }
            else if (strchr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ word->loop case, i: %d }}\n", tk->i);
                    printf("{{ tk->string[tk->i+1]: %c }}\n", tk->string[tk->i]);
                #endif
                tk->state = WORD;
                tk->i += 1;
            }
            else {
                #ifdef DEBUG
                    printf("{{ word->delimiter case, i: %d }}\n", tk->i);
                #endif
                break;
            }
        }
        //ZERO
        else if (tk->state == ZERO) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ zero->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = DEC;
                break;
            }
            else if (isOctal(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ zero->octal case, i: %d }}\n", tk->i);
                #endif
                tk->i +=1;
                tk->state = OCTAL;
            }
            else if (isDotAndDigit(tk->string[tk->i+1], tk->string[tk->i+2])) {
                #ifdef DEBUG
                    printf("{{ zero->.digit case, i: %d }}\n", tk->i);
                #endif
                tk->i +=2;
                tk->state = FLOAT;
            }
            else if (isXAndHex(tk->string[tk->i+1], tk->string[tk->i+2])) {
                #ifdef DEBUG
                    printf("{{ zero->xhex case, i: %d }}\n", tk->i);
                #endif
                tk->i +=2;
                tk->state = HEX;
            }
            else if ( (tk->string[tk->i+1] == '8') || (tk->string[tk->i+1] == '9') ){ 
                #ifdef DEBUG
                    printf("{{ zero->dec case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = DEC;              
            }   
            else {
                #ifdef DEBUG
                    printf("{{ zero->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = DEC;
                break;                
            }                   
        }
        //OCTAL
        else if (tk->state == OCTAL) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ octal->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = OCTAL;
                break;
            }
            else if (isOctal(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ octal->loop case, i: %d }}\n", tk->i);
                #endif
                tk->i +=1;
                tk->state = OCTAL;
            }
            else if ( (tk->string[tk->i+1] == '8') || (tk->string[tk->i+1] == '9') ){ 
                #ifdef DEBUG
                    printf("{{ octal->dec case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = DEC;              
            }
            else {
                #ifdef DEBUG
                    printf("{{ octal->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = OCTAL;
                break;                
            }              
        }
        //HEX
        else if (tk->state == HEX) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ hex->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = HEX;
                break;
            }
            else if (isHex(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ hex->loop case, i: %d }}\n", tk->i);
                #endif
                tk->i +=1;
                tk->state = HEX;
            }
            else {
                #ifdef DEBUG
                    printf("{{ hex->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = HEX;
                break;                
            }              
        }
        //DEC
        else if (tk->state == DEC) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ dec->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = DEC;
                break;
            }
            else if (isDigit(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ dec->loop case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = DEC;                
            }
            else if (isDotAndDigit(tk->string[tk->i+1], tk->string[tk->i+2])) {
                #ifdef DEBUG
                    printf("{{ dec->.digit case, i: %d }}\n", tk->i);
                #endif
                tk->i +=2;
                tk->state = FLOAT;
            }
            else {
                #ifdef DEBUG
                    printf("{{ dec->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = DEC;
                break;                
            }                   
        }
        //FLOAT
        else if (tk->state == FLOAT) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ float->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = FLOAT;
                break;
            }
            else if (isDigit(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ float->loop case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = FLOAT;                
            }
            else if ( (tk->string[tk->i+1] == 'e') ||
                      (tk->string[tk->i+1] == 'E') )  {
                #ifdef DEBUG
                    printf("{{ float->floatE case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = FLOATE;
            }
            else {
                #ifdef DEBUG
                    printf("{{ float->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = FLOAT;
                break;                
            }                   
        }
        //FLOATE
        else if (tk->state == FLOATE) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ floatE->end case, i: %d }}\n", tk->i);
                #endif
                tk->i -= 1;
                tk->state = FLOAT;
                break;
            }
            else if (isPlusMinusAndDigit(tk->string[tk->i+1], tk->string[tk->i+2])) {
                #ifdef DEBUG
                    printf("{{ floatE-> +- floatD case, i: %d }}\n", tk->i);
                #endif
                tk->i += 2;
                tk->state = FLOATD;
            }
            else if (isDigit(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ floatE->floatD case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = FLOATD;                
            }
            else {
                #ifdef DEBUG
                    printf("{{ floatE->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->i -= 1;
                tk->state = FLOAT;
                break;                
            }                   
        }
        //FLOATD
        else if (tk->state == FLOATD) {
            if (tokenReachedEnd(tk)) {
                #ifdef DEBUG
                    printf("{{ floatD->end case, i: %d }}\n", tk->i);
                #endif
                tk->state = FLOAT;
                break;
            }
            else if (isDigit(tk->string[tk->i+1])) {
                #ifdef DEBUG
                    printf("{{ floatD->loop case, i: %d }}\n", tk->i);
                #endif
                tk->i += 1;
                tk->state = FLOATD;                
            }
            else {
                #ifdef DEBUG
                    printf("{{ floatD->delimiter case, i: %d }}\n", tk->i);
                #endif
                tk->state = FLOAT;
                break;                
            }                   
        }
        else if (tk->state == OPERATOR) {
            #ifdef DEBUG
                printf("{{ OPERATOR case, i: %d }}\n", tk->i);
                printf("**EXITING TKGetNextToken()**\n");
            #endif
            if (isOperator(tk->string[tk->i], tk->string[tk->i+1]) == 2) {
                tk->i += 2;
                return createOperatorToken(tk->string[tk->i-2], tk->string[tk->i-1]);     
            }
            else {
                tk->i += 1;
                return createOperatorToken(tk->string[tk->i - 1], 0);
            }
        }
        //NULL
        else {
            printf("ERROR: In TKGetNextToken, no state was reached\n");
            return NULL;
        }
    }

    char* newTokenStr = (char*)malloc(sizeof(char) *
                                      (tk->i - tokenStart + 2));
    int newTokenStrLen = tk->i - tokenStart + 1;
    strncpy(newTokenStr, tk->string+tokenStart, newTokenStrLen);
    newTokenStr[newTokenStrLen] = '\0';

    #ifdef DEBUG
        printf("{{ tokenStart: %d}}\n", tokenStart);
        printf("{{ tokenEnd: %d}}\n", tk->i);
        printf("{{ newTokenStr: %s}}\n", newTokenStr);
        int len = strlen(newTokenStr);
        printf("{{ newTokenStr len: %d}}\n", len);
        printf("**EXITING TKGetNextToken()**\n");
    #endif

    tk->i = tk->i + nextTokenDistance;
    return newTokenStr;
}




//--------------------[START: FUNCTION TESTING]--------------------//

void setIndex(TokenizerT* tk, int i) {
    (tk->i) = i;

    return;
}

void runFunctionTests() {
    int pass;
    TokenizerT* tk;
    char* testToken;
    char* testStr;
    char c;
    printf("\n\n------------------------------\n");
    printf("**BEGIN FUNCTION TESTING**\n");
    printf("------------------------------\n\n");

    /*
     * setNewTokenState()
     */
    printf("\n\n\n  setNewTokenState()  \n");

        //sets to ENDOFSTRING when at end of string
        tk = TKCreate("test");
        setIndex(tk, 4);
        setNewTokenState(tk);
        if (tk->state == ENDOFSTRING) {
            printf("sets to ENDOFSTRING when at end of string>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to ENDOFSTRING when at end of string>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to WORD when a char (first char)
        tk = TKCreate("test");
        setIndex(tk, 0);
        setNewTokenState(tk);
        if (tk->state == WORD) {
            printf("sets to WORD when a char (first char)>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to WORD when a (first char)>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to WORD when a char (middle char)
        tk = TKCreate("test");
        setIndex(tk, 2);
        setNewTokenState(tk);
        if (tk->state == WORD) {
            printf("sets to WORD when a char (middle char)>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to WORD when a (middle char)>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to WORD when a char (last char)
        tk = TKCreate("test");
        setIndex(tk, 3);
        setNewTokenState(tk);
        if (tk->state == WORD) {
            printf("sets to WORD when a char (last char)>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to WORD when a (last char)>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to WHITESPACE when 1 blank space
        tk = TKCreate(" ");
        setNewTokenState(tk);
        if (tk->state == WHITESPACE) {
            printf("sets to WHITESPACE when 1 blank space>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to WHITESPACE when 1 blank space>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to ERROR when start-of-header
        char strSOH[2] = {1, '\0'};
        tk = TKCreate(strSOH);
        setNewTokenState(tk);
        if (tk->state == ERROR) {
            printf("sets to ERROR when start-of-header>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to ERROR when start-of-header>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to ERROR when @
        char strAt[2] = {'@', '\0'};
        tk = TKCreate(strAt);
        setNewTokenState(tk);
        if (tk->state == ERROR) {
            printf("sets to ERROR when @>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to ERROR when @>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

        //sets to ZERO when a 0
        tk = TKCreate("te0st");
        setIndex(tk, 2);
        setNewTokenState(tk);
        if (tk->state == ZERO) {
            printf("sets to ZERO when a 0>>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("sets to ZERO when a 0>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        TKDestroy(tk);

    /*
     * isWhitespace
    */
    printf("\n\n\n  isWhitespace()  \n");
        
        //_ is whitespace
        testStr = (char*)malloc(sizeof(char) *
                               (1 + 1));
        testStr[0] = ' ';
        testStr[1] = '\0';
        if (isWhitespace(testStr[0])) {
            printf("_ is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("_ is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        free(testStr);
        
        //\t is whitespace
        testStr = (char*)malloc(sizeof(char) *
                               (1 + 1));
        testStr[0] = '\t';
        testStr[1] = '\0';
        if (isWhitespace(testStr[0])) {
            printf("\\t is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("\\t is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        free(testStr);

        //c=13 is whitespace
        c = 13;
        if (isWhitespace(c)) {
            printf("c=13 is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("c=13 is whitespace"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }

        //c=14 is NOT whitespace
        c = 14;
        if (!isWhitespace(c)) {
            printf("c=14 is NOT whitespace"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("c=14 is NOT whitespace"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }

        //\b is NOT whitespace
        testStr = (char*)malloc(sizeof(char) *
                               (1 + 1));
        testStr[0] = '\b';
        testStr[1] = '\0';
        if (!isWhitespace(testStr[0])) {
            printf("\\b is NOT whitespace"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("\\b is NOT whitespace"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        free(testStr);

    /*
     * isOctal
    */
    printf("\n\n\n  isOctal()  \n");
        //4 is octal
        if (isOctal('4')) {
            printf("4 is octal"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("4 is octal"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        
        //8 is NOT octal
        if (!isOctal('8')) {
            printf("8 is NOT octal"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("8 is NOT octal"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isHex
    */
    printf("\n\n\n  isHex()  \n");
        //F is Hex
        if (isHex('F')) {
            printf("F is Hex"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("F is Hex"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        
        //g is NOT Hex
        if (!isHex('g')) {
            printf("g is NOT Hex"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("g is NOT Hex"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isXAndHex
    */
    printf("\n\n\n  isXAndHex()  \n");
        //x a is X and Hex
        if (isXAndHex('x', 'a')) {
            printf("x a is X and Hex"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("x a is X and Hex"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        
        //a a is NOT X and Hex
        if (!isXAndHex('a', 'a')) {
            printf("a a is NOT X and Hex"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("a a is NOT X and Hex"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isDigit
    */
    printf("\n\n\n  isDigit()  \n");
        //5 is digit
        if (isDigit('5')) {
            printf("5 is Digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("5 is Digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        
        //g is NOT digit
        if (!isDigit('g')) {
            printf("g is NOT Digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("g is NOT Digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isPlusMinusAndDigit
    */
    printf("\n\n\n  isPlusMinusAndDigit()  \n");
        //+ 0 is +- and digit
        if (isPlusMinusAndDigit('+', '0')) {
            printf("+ 0 is +- and digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("+ 0 is +- and digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }

        //+ a is NOT +- and digit
        if (!isPlusMinusAndDigit('+', 'a')) {
            printf("+ a is NOT +- and digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("+ a is NOT +- and digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isDotAndDigit
    */
    printf("\n\n\n  isDotAndDigit()  \n");
        //. 0 is . and digit
        if (isDotAndDigit('.', '0')) {
            printf(". 0 is . and digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf(". 0 is . and digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
                
        //. z is NOT . and digit
        if (!isDotAndDigit('.', 'z')) {
            printf(". z is NOT . and digit"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf(". z is NOT . and digit"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
    /*
     * isABC
    */
    printf("\n\n\n  isABC()  \n");
        //a is ABC
        if (isABC('a')) {
            printf("a is ABC"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("a is ABC"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }
        
        //9 is NOT ABC
        if (!isABC('9')) {
            printf("9 is NOT ABC"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("9 is NOT ABC"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");            
        }


    /*
     * TKGetNextToken()
     */
    printf("\n\n\n  TKGetNextToken()  \n");
        //string 'test' returns 'test', WORD token, len 4, \0
        tk = TKCreate("test");
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "test") == 0) ) {
            printf("string test did not return test\n");
            pass = 0;
        }
        if ( !(tk->state == WORD) ) {
            printf("state was not WORD\n");
            pass = 0;
        }
        if ( !(strlen(testToken) == 4) ) {
            printf("len was not 4\n");
            pass = 0;
        }
        if ( !(testToken[4] == '\0') ) {
            printf("not null-terminated\n");
            pass = 0;
        }
        if (pass) {
            printf("string test returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string test returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string 't' returns 't', WORD token, len 1, \0
        tk = TKCreate("t");
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "t") == 0) ) {
            printf("string t did not return t\n");
            pass = 0;
        }
        if ( !(tk->state == WORD) ) {
            printf("state was not WORD\n");
            pass = 0;
        }
        if ( !(strlen(testToken) == 1) ) {
            printf("len was not 1\n");
            pass = 0;
        }
        if ( !(testToken[1] == '\0') ) {
            printf("not null-terminated\n");
            pass = 0;
        }
        if (pass) {
            printf("string t returns t, WORD token, len 1, null-t"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string t returns t, WORD token, len 1 null-t"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string 'test ' returns 'test', WORD token, len 4, \0
        tk = TKCreate("test ");
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "test") == 0) ) {
            printf("string test5test did not return test\n");
            pass = 0;
        }
        if ( !(tk->state == WORD) ) {
            printf("state was not WORD\n");
            pass = 0;
        }
        if ( !(strlen(testToken) == 4) ) {
            printf("len was not 4\n");
            pass = 0;
        }
        if ( !(testToken[4] == '\0') ) {
            printf("not null-terminated\n");
            pass = 0;
        }
        if (pass) {
            printf("string test_ returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string test_ returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string '@' returns '(error)' and leaves an ERROR state
        tk = TKCreate(strAt);
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "(error)") == 0) ) {
            printf("string @ did not return (error)\n");
            pass = 0;
        }
        if ( !(tk->state == ERROR) ) {
            printf("state was not ERROR\n");
            pass = 0;
        }
        if (pass) {
            printf("string '@' returns '(error)' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string '@' returns '(error)' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string '(STX)' returns '(error)[0x02]' and leaves an ERROR state
        char strSTX[2] = {2, '\0'};
        tk = TKCreate(strSTX);
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "(error)[0x02]") == 0) ) {
            printf("string 'STX' did  return (error)[0x02]\n");
            pass = 0;
        }
        if ( !(tk->state == ERROR) ) {
            printf("state was not ERROR\n");
            pass = 0;
        }
        if (pass) {
            printf("string '(STX)' returns '(error)[0x02]' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string '(STX)' returns '(error)[0x02]' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string '(STX)' DOES NOT return '(error)[0x01]' and leaves an ERROR state
        char strSTX2[2] = {2, '\0'};
        tk = TKCreate(strSTX2);
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( (strcmp(testToken, "(error)[0x01]") == 0) ) {
            printf("string 'STX' did not return (error)\n");
            pass = 0;
        }
        if ( !(tk->state == ERROR) ) {
            printf("state was not ERROR\n");
            pass = 0;
        }
        if (pass) {
            printf("string '(STX)' DOES NOT return '(error)[0x01]' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string '(STX)' DOES NOT return '(error)[0x01]' and leaves an ERROR state"
                   ")>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string '(FN)' returns '(error)' and leaves an ERROR state
        char strFN[2] = {159, '\0'};
        tk = TKCreate(strFN);
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "(error)") == 0) ) {
            printf("string 'FN' did not return (error)\n");
            pass = 0;
        }
        if ( !(tk->state == ERROR) ) {
            printf("state was not ERROR\n");
            pass = 0;
        }
        if (pass) {
            printf("string '(FN)' returns '(error)' and leaves an ERROR state"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string '(FN)' returns '(error)' and leaves an ERROR state"
                   ")>>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string 'test ' returns 'test', WORD token, len 4, \0
        tk = TKCreate("test ");
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "test") == 0) ) {
            printf("string test5test did not return test\n");
            pass = 0;
        }
        if ( !(tk->state == WORD) ) {
            printf("state was not WORD\n");
            pass = 0;
        }
        if ( !(strlen(testToken) == 4) ) {
            printf("len was not 4\n");
            pass = 0;
        }
        if ( !(testToken[4] == '\0') ) {
            printf("not null-terminated\n");
            pass = 0;
        }
        if (pass) {
            printf("string test_ returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string test_ returns test, WORD token, len 4, null-t"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);

        //string '0' returns '0', DEC token, len 1, \0
        tk = TKCreate("0");
        testToken = TKGetNextToken(tk);
        pass = 1;
        if ( !(strcmp(testToken, "0") == 0) ) {
            printf("string test did not return test\n");
            pass = 0;
        }
        if ( !(tk->state == DEC) ) {
            printf("state was not DEC\n");
            pass = 0;
        }
        if ( !(strlen(testToken) == 1) ) {
            printf("len was not 1\n");
            pass = 0;
        }
        if ( !(testToken[1] == '\0') ) {
            printf("not null-terminated\n");
            pass = 0;
        }
        if (pass) {
            printf("string '0' returns '0', DEC token, len 1, null-t"
                   ">>>>>>>>>>>>>>>>>>>>PASSED\n");
        }
        else {
            printf("string '0' returns '0', DEC token, len 1, null-t"
                   ">>>>>>>>>>>>>>>>>>>>FAILED\n");
        }
        free(testToken);
        TKDestroy(tk);


    printf("\n\n------------------------------\n");
    printf("**END FUNCTION TESTING**\n");
    printf("------------------------------\n\n");

    return;
}


//--------------------[END: FUNCTION TESTING]--------------------//





/*
 * main will have a string argument (in argv[1]).
 * The string argument contains the tokens.
 * Print out the tokens in the second string in left-to-right order.
 * Each token should be printed on a separate line.
 */
int main(int argc, char **argv) {

    //CHECK ARGUMENTS
    if (argc != 2) {
        printf("ERROR: incorrect number of arguments, terminating\n");
        return 0;
    }
    if (strlen(argv[1]) <= 0) {
        printf("ERROR: no string to tokenize, terminating\n");
        return 0;   
    }

    //COPY STRING ARG (as per directions)
    char* tString = (char*)malloc(sizeof(char) * 
                                 (strlen(argv[1]) + 1));
    strcpy(tString, argv[1]);

    //RUN FUNCTION TESTS
    int runFunctionTestsBool = 0;
    if (runFunctionTestsBool) {
        runFunctionTests(tString);
    }

    //BEGIN MAIN FUNCTION
    #ifdef DEBUG
        printf("**ENTERING main()**\n");
        printf("{{ tString: %s }}\n", tString);
        int size = strlen(tString);
        printf("{{ tString len: %d }}\n", size);
    #endif

    TokenizerT* tk = TKCreate(tString);


    int tokenizingLoop = 1;
    while (tokenizingLoop) {
        tk->tokenString = TKGetNextToken(tk);
        if (tk->state == ENDOFSTRING) {
            printf(pRED "(reached end of string, tokenizing complete)\n" pRESET);
            break;
        }
        else if (tk->tokenString == NULL) {
            printf("ERROR: TKGetNextToken returned NULL\n");
            return 0;
        }
        else if (tk->state == ERROR) {
            printToken("", tk);
        }
        else if (tk->state == WORD) {
            printToken("word", tk);
        }
        else if (tk->state == OCTAL) {
            printToken("octal", tk);
        }
        else if (tk->state == HEX) {
            printToken("hex", tk);
        }
        else if (tk->state == DEC) {
            printToken("decimal integer", tk);
        }
        else if (tk->state == FLOAT) {
            printToken("float", tk);
        }
        else if (tk->state == OPERATOR) {
            char* operatorType = createOperatorTokenType(
                                 tk->tokenString[0], tk->tokenString[1]);
            printToken(operatorType, tk);
            free(operatorType);
        }

        if (tk->state != WHITESPACE) {
            free(tk->tokenString);
        }
    }

    TKDestroy(tk);
    free(tString);
    return 0;
}